# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Netbrain System. The API that was used to build the adapter for Netbrain is usually available in the report directory of this adapter. The adapter utilizes the Netbrain API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Netbrain adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Netbrain.

With this adapter you have the ability to perform operations with Netbrain on items such as:

- Sites
- Map
- Inventory

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
