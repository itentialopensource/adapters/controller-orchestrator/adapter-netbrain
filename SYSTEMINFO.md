# NetBrain

Vendor: NetBrain
Homepage: https://www.netbraintech.com/

Product: NetBrain
Product Page: https://www.netbraintech.com/

## Introduction
We classify Netbrain into the Data Center and Network Services domains as it helps network engineers automate key tasks, reduce outages, and ensure network security.

## Why Integrate
The Netbrain adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Netbrain.

With this adapter you have the ability to perform operations with Netbrain on items such as:

- Sites
- Map
- Inventory

## Additional Product Documentation
The [API documents for Netbrain](https://www.netbraintech.com/docs/ie101/help/restful-api.htm)
