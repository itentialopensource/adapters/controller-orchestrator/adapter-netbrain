
## 1.1.1 [09-06-2022]

* Update Healthcheck and change tenantId Usage

See merge request itentialopensource/adapters/controller-orchestrator/adapter-netbrain!16

---

## 1.1.0 [05-22-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-netbrain!15

---

## 1.0.3 [08-19-2021]

- Update README and propertiesSchema

See merge request itentialopensource/adapters/controller-orchestrator/adapter-netbrain!14

---

## 1.0.2 [08-19-2021]

- Try to fix audit issues

See merge request itentialopensource/adapters/controller-orchestrator/adapter-netbrain!13

---

## 1.0.1 [07-19-2021]

- Add the default to the getToken call

See merge request itentialopensource/adapters/controller-orchestrator/adapter-netbrain!11

---

## 1.0.0 [05-14-2021]

- Added undocumented options params to getV1CMDBDevices function.

See merge request itentialopensource/adapters/controller-orchestrator/adapter-netbrain!10

---

## 0.5.3 [03-11-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/controller-orchestrator/adapter-netbrain!9

---

## 0.5.2 [11-30-2020]

- Migrated the adapter and added new properties for tenantId and domainId

See merge request itentialopensource/adapters/controller-orchestrator/adapter-netbrain!8

---

## 0.5.1 [07-08-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-netbrain!7

---

## 0.5.0 [04-23-2020]

- Make changes to authentication and add in process for associating the token to the current domain and then logging out.

See merge request itentialopensource/adapters/controller-orchestrator/adapter-netbrain!6

---

## 0.4.0 [04-17-2020]

- Update to the latest swagger for NetBrain and also address Authentication so it is easier for customers to start using this

See merge request itentialopensource/adapters/controller-orchestrator/adapter-netbrain!5

---

## 0.3.1 [01-13-2020]

- Bring up to the latest adapter foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-netbrain!4

---

## 0.3.0 [11-08-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/controller-orchestrator/adapter-netbrain!3

---

## 0.2.3 [09-16-2019] & 0.2.2 [07-30-2019] & 0.2.1 [07-29-2019]

- Migrated and testing adapter installation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-netbrain!2

---

## 0.2.0 [07-17-2019]

- Migrate the adapter to the latest adapter foundation, categorize and prepare for app artifact

See merge request itentialopensource/adapters/controller-orchestrator/adapter-netbrain!1

---

## 0.1.1 [04-25-2019]

- Initial Commit

See commit d284f59

---
