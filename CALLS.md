## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Netbrain. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Netbrain.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the NetBrain. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">processRequest(entity, action, reqObj, returnFlag, retryNum, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/Session/CurrentDomain?{query}</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV1Session(body, callback)</td>
    <td style="padding:15px">Logout from session</td>
    <td style="padding:15px">{base_path}/{version}/Session?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV1SessionCurrentDomain(request, callback)</td>
    <td style="padding:15px">Associate a tenant ID and domain ID to the current session</td>
    <td style="padding:15px">{base_path}/{version}/Session/CurrentDomain?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getcmdbChangeAnalysisExportTasksStatus(taskId, iapMetadata, callback)</td>
    <td style="padding:15px">getcmdbChangeAnalysisExportTasksStatus</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/ChangeAnalysis/Export/Tasks/{pathv1}/Status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postcmdbChangeAnalysisExportTasks(body, iapMetadata, callback)</td>
    <td style="padding:15px">postcmdbChangeAnalysisExportTasks</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/ChangeAnalysis/Export/Tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getcmdbChangeAnalysisExportTasks12345Download(iapMetadata, callback)</td>
    <td style="padding:15px">getcmdbChangeAnalysisExportTasks12345Download</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/ChangeAnalysis/Export/Tasks/12345/Download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postcmdbCMTasksScheduledTask(body, iapMetadata, callback)</td>
    <td style="padding:15px">postcmdbCMTasksScheduledTask</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/CM/Tasks/ScheduledTask?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getcmdbCMTasksScheduledTask(iapMetadata, callback)</td>
    <td style="padding:15px">getcmdbCMTasksScheduledTask</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/CM/Tasks/ScheduledTask?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putcmdbCMTasksScheduledTask(body, iapMetadata, callback)</td>
    <td style="padding:15px">putcmdbCMTasksScheduledTask</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/CM/Tasks/ScheduledTask?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletecmTasksScheduleTask(runbookId, iapMetadata, callback)</td>
    <td style="padding:15px">deletecmTasksScheduleTask</td>
    <td style="padding:15px">{base_path}/{version}/CM/Tasks/ScheduleTask?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postcmBinding(body, iapMetadata, callback)</td>
    <td style="padding:15px">postcmBinding</td>
    <td style="padding:15px">{base_path}/{version}/CM/Binding?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postcmApprovalState(body, iapMetadata, callback)</td>
    <td style="padding:15px">postcmApprovalState</td>
    <td style="padding:15px">{base_path}/{version}/CM/Approval/State?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postcmdbDeviceAccessPolicy(body, iapMetadata, callback)</td>
    <td style="padding:15px">postcmdbDeviceAccessPolicy</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/DeviceAccessPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getcmdbDeviceAccessPolicy(name, iapMetadata, callback)</td>
    <td style="padding:15px">getcmdbDeviceAccessPolicy</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/DeviceAccessPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putcmdbDeviceAccessPolicy(body, iapMetadata, callback)</td>
    <td style="padding:15px">putcmdbDeviceAccessPolicy</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/DeviceAccessPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postcmdbUserDeviceAccessPolicyAssignDeviceAccessPolicies(body, iapMetadata, callback)</td>
    <td style="padding:15px">postcmdbUserDeviceAccessPolicyAssignDeviceAccessPolicies</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/UserDeviceAccessPolicy/AssignDeviceAccessPolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletecmdbDeviceAccessPolicy(deviceGroup, iapMetadata, callback)</td>
    <td style="padding:15px">deletecmdbDeviceAccessPolicy</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/DeviceAccessPolicy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getcmdbUserDeviceAccessPolicy(authenticationServer, userName, iapMetadata, callback)</td>
    <td style="padding:15px">getcmdbUserDeviceAccessPolicy</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/UserDeviceAccessPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletecmdbUserDeviceAccessPolicy(iapMetadata, callback)</td>
    <td style="padding:15px">deletecmdbUserDeviceAccessPolicy</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/UserDeviceAccessPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postcmdbUserDeviceAccessPolicy(body, iapMetadata, callback)</td>
    <td style="padding:15px">postcmdbUserDeviceAccessPolicy</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/UserDeviceAccessPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getcmdbUserDeviceAccessPolicyUsersOfDeviceAccessPolicy(name, iapMetadata, callback)</td>
    <td style="padding:15px">getcmdbUserDeviceAccessPolicyUsersOfDeviceAccessPolicy</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/UserDeviceAccessPolicy/UsersOfDeviceAccessPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postcmdbDeviceGroups(body, iapMetadata, callback)</td>
    <td style="padding:15px">postcmdbDeviceGroups</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/DeviceGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postcmdbDeviceGroupsDevices(id, body, iapMetadata, callback)</td>
    <td style="padding:15px">postcmdbDeviceGroupsDevices</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/DeviceGroups/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletecmdbDeviceGroupsDevices(id, iapMetadata, callback)</td>
    <td style="padding:15px">deletecmdbDeviceGroupsDevices</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/DeviceGroups/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getcmdbDeviceGroupsDevices(id, iapMetadata, callback)</td>
    <td style="padding:15px">getcmdbDeviceGroupsDevices</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/DeviceGroups/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletecmdbDeviceGroups(id, iapMetadata, callback)</td>
    <td style="padding:15px">deletecmdbDeviceGroups</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/DeviceGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postcmdbInterfacesAttributes(body, iapMetadata, callback)</td>
    <td style="padding:15px">postcmdbInterfacesAttributes</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/Interfaces/Attributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletecmdbDiscoveryTasks(task, iapMetadata, callback)</td>
    <td style="padding:15px">deletecmdbDiscoveryTasks</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/Discovery/Tasks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putcmdbDiscoveryTasks(body, iapMetadata, callback)</td>
    <td style="padding:15px">putcmdbDiscoveryTasks</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/Discovery/Tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postcmdbDiscoveryTasks(body, iapMetadata, callback)</td>
    <td style="padding:15px">postcmdbDiscoveryTasks</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/Discovery/Tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getcmdbDiscoveryTasksResults(task, iapMetadata, callback)</td>
    <td style="padding:15px">getcmdbDiscoveryTasksResults</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/Discovery/Tasks/{pathv1}/Results?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletecmdbEventConsole(iapMetadata, callback)</td>
    <td style="padding:15px">deletecmdbEventConsole</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/EventConsole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getcmdbEventConsole(eventType, eventLevel, iapMetadata, callback)</td>
    <td style="padding:15px">getcmdbEventConsole</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/EventConsole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postcmdbIncidentList(body, iapMetadata, callback)</td>
    <td style="padding:15px">postcmdbIncidentList</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/Incident/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getcmdbNetworkSettingsTelnetInfoCheckDeviceCount(alias, iapMetadata, callback)</td>
    <td style="padding:15px">getcmdbNetworkSettingsTelnetInfoCheckDeviceCount</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/NetworkSettings/TelnetInfo/CheckDeviceCount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getcmdbNetworkSettingsTelnetInfoRefreshDeviceCount(iapMetadata, callback)</td>
    <td style="padding:15px">getcmdbNetworkSettingsTelnetInfoRefreshDeviceCount</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/NetworkSettings/TelnetInfo/RefreshDeviceCount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putcmdbSitesLeafDynamicSearch(body, iapMetadata, callback)</td>
    <td style="padding:15px">putcmdbSitesLeafDynamicSearch</td>
    <td style="padding:15px">{base_path}/{version}/CMDB/Sites/Leaf/DynamicSearch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
